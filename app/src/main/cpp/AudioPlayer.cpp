//
// Created by bortek on 2018-12-02.
//

#include "AudioPlayer.h"
#include "Clock.h"
#include <cstdlib>
#include <unistd.h>
#include <android/log.h>
#include <cstring>
#include <ctime>
#include <exception>

#define SENDING_MSG_INTERVAL 10

bool AudioPlayer::playingState = false;
bool AudioPlayer::isMaster = false;

void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *ctx)
{
    (static_cast<AudioPlayer *> (ctx))->bqAudioPlayerCallback(bq);
}

AudioPlayer::AudioPlayer(SLEngineItf slEngineItf, Buffers::Buff *sndBuff) {

    buff = sndBuff;
    genBuffPlyManager = buff->buff;

    res = (*slEngineItf)->CreateOutputMix(slEngineItf, &outputMixObjectItf, 0, NULL, NULL);
    slResultAssert(res);

    res = (*outputMixObjectItf)->Realize(outputMixObjectItf, SL_BOOLEAN_FALSE);
    slResultAssert(res);

    SLDataLocator_AndroidSimpleBufferQueue loc_buf = {
            SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2
    };

    SLAndroidDataFormat_PCM_EX format_pcm_ex;
    memset(&format_pcm_ex, 0, sizeof(format_pcm_ex));
    format_pcm_ex.formatType = SL_ANDROID_DATAFORMAT_PCM_EX;
    format_pcm_ex.representation = SL_ANDROID_PCM_REPRESENTATION_SIGNED_INT;
    format_pcm_ex.numChannels = 1;
    format_pcm_ex.channelMask = SL_SPEAKER_FRONT_LEFT;
    format_pcm_ex.sampleRate = SL_SAMPLINGRATE_16;
    format_pcm_ex.endianness = SL_BYTEORDER_LITTLEENDIAN;
    format_pcm_ex.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
    format_pcm_ex.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;

    SLDataSource audioSrc = {&loc_buf, &format_pcm_ex};
    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixObjectItf};
    SLDataSink audioSnk = {&loc_outmix, NULL};

    SLInterfaceID  ids[2] = {SL_IID_BUFFERQUEUE, SL_IID_VOLUME};
    SLboolean req[2] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_FALSE};


    res = (*slEngineItf)->CreateAudioPlayer(slEngineItf, &playerObjectItf, &audioSrc, &audioSnk,
            sizeof(ids) / sizeof(ids[0]), ids, req);
    slResultAssert(res);

    res = (*playerObjectItf)->Realize(playerObjectItf, SL_BOOLEAN_FALSE);
    slResultAssert(res);

    res = (*playerObjectItf)->GetInterface(playerObjectItf, SL_IID_PLAY, &playItf);
    slResultAssert(res);

    res = (*playerObjectItf)->GetInterface(playerObjectItf, SL_IID_BUFFERQUEUE, &playBufferQueueItf);
    slResultAssert(res);

    res = (*playItf)->SetPlayState(playItf, SL_PLAYSTATE_STOPPED);
    slResultAssert(res);

    res = (*playBufferQueueItf)->RegisterCallback(playBufferQueueItf, &bqPlayerCallback, this);
    slResultAssert(res);
}

void AudioPlayer::bqAudioPlayerCallback(SLAndroidSimpleBufferQueueItf bq)
{
    (*bq)->Enqueue(bq, genBuffPlyManager, buff->buffSize);
    res = (*playItf)->SetPlayState(playItf, SL_PLAYSTATE_STOPPED);
    AudioPlayer::playingState = false;
}

void AudioPlayer::startPlay(bool isMaster)
{
    AudioPlayer::isMaster = isMaster;
    bool helpIsMaster = isMaster;
    while(AudioEngine::isRunning)
    {
        if (helpIsMaster || Buffers::allowSnd) {
            res = (*playBufferQueueItf)->Enqueue(playBufferQueueItf, genBuffPlyManager,
                                                 buff->buffSize);
            slResultAssert(res);
            break;
        }
    }
    while(AudioEngine::isRunning)
    {
        if(helpIsMaster || Buffers::allowSnd)
        {
            __android_log_print(ANDROID_LOG_INFO, "bqAudioRecorder", "Start clock");
            Clock::startClock();
            AudioPlayer::playingState = true;
            res = (*playItf)->SetPlayState(playItf, SL_PLAYSTATE_PLAYING);
            Buffers::allowSnd = false;
            helpIsMaster = false;
        }
        if(!AudioEngine::isRunning)
            std::terminate();
    }
}



void AudioPlayer::slResultAssert(SLresult res)
{
    if( SL_RESULT_SUCCESS != res)
        exit(1);
}

AudioPlayer::~AudioPlayer()
{
    (*playerObjectItf)->Destroy(playerObjectItf);
    (*outputMixObjectItf)->Destroy(outputMixObjectItf);
}
