//
// Created by bortek on 2019-01-17.
//

#include <cstring>
#include <string>
#include <android/log.h>
#include <cmath>
#include <zconf.h>
#include "Correlation.h"
#include "AudioPlayer.h"
#include "Clock.h"

Correlation::Correlation(int16_t *sender, size_t senderSize)
{
    copy_size = senderSize;
    snd = new int16_t[copy_size];
    rec = new int16_t[copy_size];
    memcpy(snd, sender, copy_size * sizeof(int16_t));
}

Correlation::~Correlation() {
    delete [] snd;
    delete [] rec;
}

void Correlation::start(int16_t *receive)
{
    memcpy(rec, receive, copy_size * sizeof(int16_t));
    pearsoncoeff(snd, rec, copy_size);
}

double Correlation::sumInt16(int16_t a[], uint32_t sizeA)
{
    double s = 0;
    for (int i = 0; i < sizeA; i++)
    {
        s = s + a[i];
    }
    return s;
}

double Correlation::sumInt32(int32_t a[], uint32_t sizeA)
{
    double s = 0;
    for (int i = 0; i < sizeA; i++)
    {
        s = s + a[i];
    }
    return s;
}

double Correlation::mean(int16_t a[], uint32_t sizeA)
{
    return sumInt16(a, sizeA) / sizeA;
}

double Correlation::sqsum(int16_t a[], uint32_t sizeA)
{
    double s = 0;
    for (int i = 0; i < sizeA; i++)
    {
        s = s + pow(a[i], 2);
    }
    return s;
}

double Correlation::stdev(int16_t nums[], uint32_t sizeNums)
{
    return pow(sqsum(nums, sizeNums) / sizeNums - pow(sumInt16(nums, sizeNums) / sizeNums, 2), 0.5);
}

double Correlation::pearsoncoeff(int16_t X[], int16_t Y[], uint32_t sizeXY)
{
    double xcorr = 0;
    double stdevX = stdev(X, sizeXY);
    double stdevY = stdev(Y, sizeXY);
    double meanX = mean(X, sizeXY);
    double meanY = mean(Y, sizeXY);
    int32_t *valXY = new int32_t[sizeXY];
    for(uint32_t i = 0; i < sizeXY; i++)
    {
        X[i] = X[i] - meanX;
        Y[i] = Y[i] - meanY;
        valXY[i] = X[i] * Y[i];
    }
    xcorr = sumInt32(valXY, sizeXY) / (sizeXY * stdevX * stdevY);
    checkXCorr(xcorr);
    __android_log_print(ANDROID_LOG_INFO, "bqAudioRecorder", "xCorr: %f", xcorr);
    delete [] valXY;
    return xcorr;
}

void Correlation::checkXCorr(double xCorr)
{
    Buffers::isCorr = false;
    if(xCorr > 0.02)
        Buffers::isCorr = true;
}
