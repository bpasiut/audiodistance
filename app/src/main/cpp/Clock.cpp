//
// Created by bortek on 2019-01-18.
//

#include "Clock.h"

std::chrono::steady_clock::time_point Clock::start = std::chrono::steady_clock::now();
std::chrono::steady_clock::time_point Clock::stop = std::chrono::steady_clock::now();

std::chrono::steady_clock::time_point Clock::startSlave = std::chrono::steady_clock::now();
std::chrono::steady_clock::time_point Clock::stopSlave = std::chrono::steady_clock::now();

Clock::Clock()
{

}

Clock::~Clock()
{

}

void Clock::startClock()
{
    Clock::start = std::chrono::steady_clock::now();
}

void Clock::stopClock()
{
    Clock::stop = std::chrono::steady_clock::now();
}

double Clock::readTimeDiff()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(Clock::stop - Clock::start).count();
}

void Clock::startClockSlave()
{
    Clock::startSlave = std::chrono::steady_clock::now();
}

void Clock::stopClockSlave()
{
    Clock::stopSlave = std::chrono::steady_clock::now();
}

double Clock::readTimeDiffSlave()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(Clock::stopSlave - Clock::startSlave).count();
}


