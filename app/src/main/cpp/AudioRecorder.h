//
// Created by bortek on 2018-12-02.
//

#ifndef AUDIODISTANCE_AUDIORECORDER_H
#define AUDIODISTANCE_AUDIORECORDER_H


#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include "AudioEngine.h"
#include "Buffers.h"

class AudioRecorder {
    SLObjectItf  recorderObjectItf;
    SLRecordItf recItf;
    SLAndroidSimpleBufferQueueItf recBufferQueueItf;
    Buffers::Buff *buff;
    int16_t *genBuffRecManager;
    SLresult res;

    const Buffers::Buff *prbsBuff;
public:
    AudioRecorder(SLEngineItf slEngineItf, Buffers::Buff *recBuff, const Buffers::Buff *sndBuff);
    ~AudioRecorder();

    void bqAudioRecorderCallback(SLAndroidSimpleBufferQueueItf bq);
    void slResultAssert(SLresult res);
    void startRecording();
    float isCorellation();
};


#endif //AUDIODISTANCE_AUDIORECORDER_H
