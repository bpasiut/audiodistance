//
// Created by bortek on 2018-12-05.
//

#include "Buffers.h"
#include <random>

bool Buffers::allowSnd = false;
bool Buffers::isCorr = false;

Buffers::Buffers(size_t recBSize)//, size_t bStep)
{
    recBuff.buffSize = recBSize;
    recBuff.maxStep = 4;
    recBuff.buff = new int16_t[recBuff.buffSize];
    recBuff.buffCount = 1;
    recBuff.buffStep = recBuff.buffSize / recBuff.maxStep;

    sndBuff.buffSize = recBuff.buffStep * 2;
    sndBuff.maxStep = 2;
    sndBuff.buff = new int16_t[sndBuff.buffSize];
    sndBuff.buffCount = 1;
    sndBuff.buffStep = sndBuff.buffSize / sndBuff.maxStep;

    maxSampleVal = 32767;
    minSampleVal = -32767;

    generateTone();
}

Buffers::~Buffers()
{
    delete [] recBuff.buff;
    delete [] sndBuff.buff;
}

void Buffers::generateRandomSequence()
{
    std::mt19937 rng;
    std::uniform_int_distribution<> dist(0,1);

    for (int i = 0; i < sndBuff.buffSize; i++)
    {
        if(dist(rng))
            sndBuff.buff[i] = maxSampleVal ? maxSampleVal : 127;
        else
            sndBuff.buff[i] = minSampleVal ? minSampleVal : -127;
    }
}

void Buffers::generateTone()
{
    for (int i = 0; i < sndBuff.buffSize; ++i)
    {
        sndBuff.buff[i] = sin(2 * M_PI * 1250 * (double(i) / 16000)) * 32767;
    }
}
