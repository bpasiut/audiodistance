//
// Created by bortek on 2018-12-02.
//

#include "AudioRecorder.h"
#include "Clock.h"
#include "AudioPlayer.h"
#include <cstdlib>
#include <unistd.h>
#include <android/log.h>
#include <cstring>
#include <cmath>
#include <exception>

void bqRecorderCallback(SLAndroidSimpleBufferQueueItf bq, void *ctx)
{
    (static_cast<AudioRecorder *> (ctx))->bqAudioRecorderCallback(bq);
}

AudioRecorder::AudioRecorder(SLEngineItf slEngineItf, Buffers::Buff *recBuff, const Buffers::Buff *sndBuff) {

    buff = recBuff;
    prbsBuff = sndBuff;
    genBuffRecManager = buff->buff;

    SLAndroidDataFormat_PCM_EX format_pcm_ex;
    memset(&format_pcm_ex, 0, sizeof(format_pcm_ex));
    format_pcm_ex.formatType = SL_ANDROID_DATAFORMAT_PCM_EX;
    format_pcm_ex.representation = SL_ANDROID_PCM_REPRESENTATION_SIGNED_INT;
    format_pcm_ex.numChannels = 1;
    format_pcm_ex.channelMask = SL_SPEAKER_FRONT_LEFT;
    format_pcm_ex.sampleRate = SL_SAMPLINGRATE_16;
    format_pcm_ex.endianness = SL_BYTEORDER_LITTLEENDIAN;
    format_pcm_ex.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
    format_pcm_ex.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;

    SLDataLocator_IODevice loc_dev = {SL_DATALOCATOR_IODEVICE,
                                      SL_IODEVICE_AUDIOINPUT,
                                      SL_DEFAULTDEVICEID_AUDIOINPUT, NULL};
    SLDataSource audioSrc = {&loc_dev, NULL};

    SLDataLocator_AndroidSimpleBufferQueue loc_buff = {
            SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 4
    };
    SLDataSink audioSnk = {&loc_buff, &format_pcm_ex};

    SLInterfaceID  ids[2] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
                             SL_IID_ANDROIDCONFIGURATION};
    SLboolean req[2] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_FALSE};

    res = (*slEngineItf)->CreateAudioRecorder(slEngineItf, &recorderObjectItf, &audioSrc, &audioSnk,
                                            sizeof(ids) / sizeof(ids[0]), ids, req);
    slResultAssert(res);

    res = (*recorderObjectItf)->Realize(recorderObjectItf, SL_BOOLEAN_FALSE);
    slResultAssert(res);

    res = (*recorderObjectItf)->GetInterface(recorderObjectItf, SL_IID_RECORD, &recItf);
    slResultAssert(res);

    res = (*recorderObjectItf)->GetInterface(recorderObjectItf, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &recBufferQueueItf);
    slResultAssert(res);

    res = (*recItf)->SetRecordState(recItf, SL_RECORDSTATE_STOPPED);
    slResultAssert(res);

    res = (*recBufferQueueItf)->RegisterCallback(recBufferQueueItf, &bqRecorderCallback, this);
    slResultAssert(res);
}

void AudioRecorder::bqAudioRecorderCallback(SLAndroidSimpleBufferQueueItf bq)
{
    if(!AudioEngine::isRunning)
        std::terminate();
    (*bq)->Enqueue(bq, genBuffRecManager, buff->buffStep);

    if(!AudioPlayer::isMaster)
        Clock::startClockSlave();
    genBuffRecManager += buff->buffStep;
    buff->buffCount += 1;

    if(buff->buffCount > buff->maxStep)
    {
        genBuffRecManager = buff->buff;
        buff->buffCount = 1;
    }
}

void AudioRecorder::startRecording()
{
    res = (*recBufferQueueItf)->Enqueue(recBufferQueueItf, genBuffRecManager, buff->buffStep);
    slResultAssert(res);
    genBuffRecManager += buff->buffStep;
    buff->buffCount += 1;

    res = (*recItf)->SetRecordState(recItf, SL_RECORDSTATE_RECORDING);
    slResultAssert(res);
}

float AudioRecorder::isCorellation()
{
    int32_t sumBuff = 0, sumPrbs = 0, sumBuffPrbs = 0;
    int32_t sqrtSumBuff = 0, sqrtSumPrbs = 0;

    for (int i = 0; i < prbsBuff->buffSize; i++)
    {
        sumBuff = sumBuff + genBuffRecManager[i];
        sumPrbs = sumPrbs + prbsBuff->buff[i];
        sumBuffPrbs = sumBuffPrbs + sumBuff + sumPrbs;

        sqrtSumBuff = sqrtSumBuff + genBuffRecManager[i] * genBuffRecManager[i];
        sqrtSumPrbs = sqrtSumPrbs + prbsBuff->buff[i] * prbsBuff->buff[i];
    }

    float corr = (float)(prbsBuff->buffSize * sumBuffPrbs - sumBuff * sumPrbs)
            / sqrt((prbsBuff->buffSize * sqrtSumBuff - sumBuff * sumBuff)
                    * (prbsBuff->buffSize * sqrtSumPrbs - sumPrbs * sumPrbs));

    return corr;
}

void AudioRecorder::slResultAssert(SLresult res)
{
    if( SL_RESULT_SUCCESS != res)
        exit(1);
}

AudioRecorder::~AudioRecorder()
{
    (*recorderObjectItf)->Destroy(recorderObjectItf);
}
