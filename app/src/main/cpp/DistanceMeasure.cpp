//
// Created by bortek on 2019-01-19.
//

#include <android/log.h>
#include <cmath>
#include "DistanceMeasure.h"
DistanceMeasure::DistanceMeasure()
{
    dist = 0;
    previouseTime = 0;
}

DistanceMeasure::~DistanceMeasure()
{

}

void DistanceMeasure::calibration(double time)
{
    cal.collectData(time);
}

double DistanceMeasure::startMeasure(double time)
{
    if(previouseTime != time)
    {
        // 0.006 time to get distance of 2 meters (one unit is one meter)
        int helpTime = time;
        dist = 330 * (float(abs(time - cal.getOneMeterTime())) - 0.006) / 2;
        previouseTime = time;
    }
    return dist;
}

size_t DistanceMeasure::getCalCounterDataTime()
{
    return cal.getCounterDataTime();
}
