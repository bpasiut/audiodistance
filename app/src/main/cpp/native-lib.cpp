//#include <string>
#include "native-lib.h"
#include "AudioEngine.h"
#include "AudioPlayer.h"
#include "AudioRecorder.h"
#include "Correlation.h"
#include "Clock.h"
#include "DistanceMeasure.h"
#include <thread>
#include <zconf.h>
#include <android/log.h>

#define REC_BUFF_SIZE 32000
#define CALIBRATION_TRIES 5

AudioEngine eng;
Buffers buff(REC_BUFF_SIZE);

Correlation corr(buff.sndBuff.buff, buff.sndBuff.buffStep);

AudioPlayer ply(eng.slEngineItf, &buff.sndBuff);
AudioRecorder rec(eng.slEngineItf, &buff.recBuff, &buff.sndBuff);

DistanceMeasure dist;

extern "C" {

void masterAudioDistance()
{
    AudioEngine::isRunning = true;
    std::thread t1(&AudioRecorder::startRecording, &rec);
    std::thread t2(&AudioPlayer::startPlay, &ply, true);
    size_t savedRecBuffCount = -1;
    while(AudioEngine::isRunning)
    {
        if(savedRecBuffCount != buff.recBuff.buffCount)
        {
            //__android_log_print(ANDROID_LOG_INFO, "bqAudioRecorder", "Rec buff count: %f", buff.recBuff.buffCount);
            if(AudioPlayer::playingState)
                sleep(3);
            int16_t *recCopyPtr = buff.recBuff.buff;
            if (buff.recBuff.buffCount == 1)
                recCopyPtr += buff.recBuff.buffStep * (buff.recBuff.maxStep - 1);
            else
                recCopyPtr += buff.recBuff.buffStep * (buff.recBuff.buffCount - 1);
            std::thread t3(&Correlation::start, &corr, recCopyPtr);
            savedRecBuffCount = buff.recBuff.buffCount;
            t3.join();
            if(Buffers::isCorr)
            {
                Clock::stopClock();
                __android_log_print(ANDROID_LOG_INFO, "bqAudioRecorder", "Time diff: %f", Clock::readTimeDiff());
                sleep(2);
                Buffers::allowSnd = true;
            }
        }
    }
}

void slaveAudioDistance()
{
    std::thread t1(&AudioRecorder::startRecording, &rec);
    std::thread t2(&AudioPlayer::startPlay, &ply, false);
    size_t savedRecBuffCount = -1;
    while(AudioEngine::isRunning)
    {
        if(savedRecBuffCount != buff.recBuff.buffCount)
        {
            if(AudioPlayer::playingState)
                sleep(3);
            int16_t *recCopyPtr = buff.recBuff.buff;
            if (buff.recBuff.buffCount == 1)
                recCopyPtr += buff.recBuff.buffStep * (buff.recBuff.maxStep - 1);
            else
                recCopyPtr += buff.recBuff.buffStep * (buff.recBuff.buffCount - 1);
            std::thread t3(&Correlation::start, &corr, recCopyPtr);
            savedRecBuffCount = buff.recBuff.buffCount;
            t3.join();
            if(Buffers::isCorr)
            {
                Clock::stopClockSlave();
                sleep(5 - Clock::readTimeDiffSlave());
                Buffers::allowSnd = true;
            }
        }
    }
}

JNIEXPORT jboolean JNICALL Java_com_example_bortek_audiodistance_MainActivity_getDistance(
        JNIEnv *env,
        jobject, /* this */
        jobject jtextViewObject) {
    while (AudioEngine::isRunning)
    {
        if(Buffers::isCorr)
        {
            jclass clazz = env->FindClass("android/widget/TextView");
            jmethodID setText = env->GetMethodID(clazz, "setText", "(Ljava/lang/CharSequence;)V");
            jstring jstr;
            if(dist.getCalCounterDataTime() < CALIBRATION_TRIES)
            {
                dist.calibration(Clock::readTimeDiff());
                jstr = env->NewStringUTF("C");
            }
            else
                jstr = env->NewStringUTF(std::to_string(dist.startMeasure(Clock::readTimeDiff())).c_str());
            env->CallVoidMethod(jtextViewObject, setText, jstr);
        }
    }

    return true;
}

JNIEXPORT jboolean JNICALL Java_com_example_bortek_audiodistance_MainActivity_startSlave(
        JNIEnv *env,
        jobject /* this */) {
    AudioEngine::isRunning = true;
    std::thread t0(&slaveAudioDistance);
    t0.detach();
    return true;
}

JNIEXPORT jboolean JNICALL Java_com_example_bortek_audiodistance_MainActivity_stopSlave(
        JNIEnv *env,
        jobject /* this */) {
    AudioEngine::isRunning = false;
    return true;
}

JNIEXPORT jboolean JNICALL Java_com_example_bortek_audiodistance_MainActivity_startMeasure(
        JNIEnv *env,
        jobject /* this */) {
    AudioEngine::isRunning = true;
    std::thread t0(&masterAudioDistance);
    t0.detach();
    return true;
}

}