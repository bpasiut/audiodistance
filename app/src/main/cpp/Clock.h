//
// Created by bortek on 2019-01-18.
//

#ifndef AUDIODISTANCE_CLOCK_H
#define AUDIODISTANCE_CLOCK_H


#include <ctime>
#include <chrono>

class Clock {
    static std::chrono::steady_clock::time_point start;
    static std::chrono::steady_clock::time_point stop;

    static std::chrono::steady_clock::time_point startSlave;
    static std::chrono::steady_clock::time_point stopSlave;
public:
    Clock();
    ~Clock();

    static void startClock();
    static void stopClock();
    static double readTimeDiff();

    static void startClockSlave();
    static void stopClockSlave();
    static double readTimeDiffSlave();
};


#endif //AUDIODISTANCE_CLOCK_H
