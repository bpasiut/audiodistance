//
// Created by bortek on 2019-01-19.
//

#include "Calibration.h"

Calibration::Calibration()
{
    dataTime = 0;
    counterDataTime = 0;
    oneMeterTime = 0;
}

Calibration::~Calibration()
{

}

size_t Calibration::collectData(double time)
{
    dataTime += time;
    return ++counterDataTime;
}

void Calibration::start()
{
    oneMeterTime = dataTime / counterDataTime;
}

double Calibration::getOneMeterTime()
{
    return oneMeterTime;
}

size_t Calibration::getCounterDataTime()
{
    return counterDataTime;
}
