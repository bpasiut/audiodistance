//
// Created by bortek on 2018-11-28.
//

#ifndef AUDIODISTANCE_NATIVE_LIB_H
#define AUDIODISTANCE_NATIVE_LIB_H
#include <jni.h>

extern "C" {
    JNIEXPORT bool JNICALL startMeasureDistance();
    JNIEXPORT bool JNICALL startEcho();
};

#endif //AUDIODISTANCE_NATIVE_LIB_H
