//
// Created by bortek on 2018-11-29.
//

#ifndef AUDIODISTANCE_AUDIOENGINE_H
#define AUDIODISTANCE_AUDIOENGINE_H

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

class AudioEngine {
public:
    static bool isRunning;

    SLObjectItf slEngineObjItf;
    SLEngineItf slEngineItf;

    AudioEngine();
    virtual ~AudioEngine();
    void slResultAssert(SLresult res);
};


#endif //AUDIODISTANCE_AUDIOENGINE_H
