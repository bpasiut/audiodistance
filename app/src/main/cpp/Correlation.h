//
// Created by bortek on 2019-01-17.
//

#ifndef AUDIODISTANCE_CORRELATION_H
#define AUDIODISTANCE_CORRELATION_H

#include "Buffers.h"

class Correlation {
    int16_t *snd;
    int16_t *rec;
    size_t copy_size;
public:
    Correlation(int16_t *sender, size_t senderSize);
    ~Correlation();

    void start(int16_t *receive);
    void checkXCorr(double xCorr);

    double sumInt16(int16_t a[], uint32_t sizeA);
    double sumInt32(int32_t a[], uint32_t sizeA);
    double mean(int16_t a[], uint32_t sizeA);
    double sqsum(int16_t a[], uint32_t sizeA);
    double stdev(int16_t nums[], uint32_t sizeNums);
    double pearsoncoeff(int16_t X[], int16_t Y[], uint32_t sizeXY);
};


#endif //AUDIODISTANCE_CORRELATION_H
