//
// Created by bortek on 2018-12-05.
//

#ifndef AUDIODISTANCE_Buffers_H
#define AUDIODISTANCE_Buffers_H


#include <cstdint>

class Buffers {
public:
    struct Buff
    {
        size_t buffSize;
        size_t maxStep;
        int16_t *buff;
        size_t buffCount;
        size_t buffStep;
    };

    Buff sndBuff;
    Buff recBuff;

    static bool allowSnd;
    static bool isCorr;
    int16_t minSampleVal;
    int16_t maxSampleVal;

    Buffers(size_t recBSize);//, size_t buffStep);
    ~Buffers();

    void generateRandomSequence();
    void generateTone();
};


#endif //AUDIODISTANCE_Buffers_H
