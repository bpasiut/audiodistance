//
// Created by bortek on 2018-12-02.
//

#ifndef AUDIODISTANCE_AUDIOPLAYER_H
#define AUDIODISTANCE_AUDIOPLAYER_H


#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include "AudioEngine.h"
#include "Buffers.h"

class AudioPlayer {
    SLObjectItf  outputMixObjectItf;
    SLObjectItf  playerObjectItf;
    SLPlayItf playItf;
    SLAndroidSimpleBufferQueueItf playBufferQueueItf;
    Buffers::Buff *buff;
    int16_t *genBuffPlyManager;
    SLresult res;

public:
    static bool playingState;
    static bool isMaster;

    AudioPlayer(SLEngineItf slEngineItf, Buffers::Buff *sndBuff);
    ~AudioPlayer();

    void bqAudioPlayerCallback(SLAndroidSimpleBufferQueueItf bq);
    void slResultAssert(SLresult res);
    void startPlay(bool isMaster);
};


#endif //AUDIODISTANCE_AUDIOPLAYER_H
