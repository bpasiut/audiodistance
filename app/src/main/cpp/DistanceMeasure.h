//
// Created by bortek on 2019-01-19.
//

#ifndef AUDIODISTANCE_DISTANCEMEASURE_H
#define AUDIODISTANCE_DISTANCEMEASURE_H

#include "Calibration.h"

class DistanceMeasure {
    Calibration cal;
    double dist;
    double previouseTime;
public:
    DistanceMeasure();
    ~DistanceMeasure();

    size_t getCalCounterDataTime();

    void calibration(double time);
    double startMeasure(double time);
};


#endif //AUDIODISTANCE_DISTANCEMEASURE_H
