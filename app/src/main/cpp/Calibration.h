//
// Created by bortek on 2019-01-19.
//

#ifndef AUDIODISTANCE_CALIBRATION_H
#define AUDIODISTANCE_CALIBRATION_H


#include <cstdint>

class Calibration {
    size_t counterDataTime;
    double dataTime;
    double oneMeterTime;
public:
    Calibration();
    ~Calibration();

    double getOneMeterTime();
    size_t getCounterDataTime();

    size_t collectData(double time);
    void start();
};


#endif //AUDIODISTANCE_CALIBRATION_H
