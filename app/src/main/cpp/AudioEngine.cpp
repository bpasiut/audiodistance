//
// Created by bortek on 2018-11-29.
//

#include <iostream>
#include "AudioEngine.h"
#include <cstdlib>

bool AudioEngine::isRunning = false;

AudioEngine::AudioEngine()
{
    AudioEngine::isRunning = true;
    SLresult  res;
    res = slCreateEngine(&slEngineObjItf, 0, NULL, 0, NULL, NULL);
    slResultAssert(res);

    res = (*slEngineObjItf)->Realize(slEngineObjItf, SL_BOOLEAN_FALSE);
    slResultAssert(res);

    res = (*slEngineObjItf)->GetInterface(slEngineObjItf, SL_IID_ENGINE, &slEngineItf);
    slResultAssert(res);
}

void AudioEngine::slResultAssert(SLresult res)
{
    if (SL_RESULT_SUCCESS != res)
    {
        std::cout << "BLAD" << std::endl;
        exit(1);
    }
}

AudioEngine::~AudioEngine()
{
    (*slEngineObjItf)->Destroy(slEngineObjItf);
}
