package com.example.bortek.audiodistance;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity
        implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int AUDIO_ECHO_REQUEST = 0;

    Button startMaster;
    Button stopMaster;
    Button startSlave;
    Button stopSlave;
    Button startMeasure;

    TextView distanceTextView;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AudioManager myAudioMgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // Example of a call to a native method

        startMaster = (Button) findViewById(R.id.startMaster);
        stopMaster = (Button) findViewById(R.id.stopMaster);
        startSlave = (Button) findViewById(R.id.startSlave);
        stopSlave = (Button) findViewById(R.id.stopSlave);
        startMeasure = (Button) findViewById(R.id.startMeasure);
        distanceTextView = (TextView) findViewById(R.id.distanceTextView);

        stopMaster.setVisibility(View.GONE);
        stopSlave.setVisibility(View.GONE);
        startMeasure.setVisibility(View.GONE);
        distanceTextView.setVisibility(View.GONE);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     **/

    public void startMasterOnClick(View view)
    {
        setMasterLayout();
    }

    public void startMeasureOnClick(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    AUDIO_ECHO_REQUEST);
            return;
            //Log.i("Record perm2: ", ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) + "");
        }
        startMeasure();
        Thread thread = new Thread()
        {
            @Override
            public void run()
            {
                getDistance(distanceTextView);
            }
        };

        thread.start();
    }

    public void startSlaveOnClick(View view) {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(
                    this,
                    new String[] { Manifest.permission.RECORD_AUDIO },
                    AUDIO_ECHO_REQUEST);
            return;
        }
        boolean status = startSlave();
        if(status == true)
            setSlaveLayout();
    }

    public void stopSlaveOnClick(View view) {
        boolean status = stopSlave();
        if(status == true)
            setMainLayoutFromSlave();
    }

    public void setMasterLayout() {
        startMaster.setVisibility(View.GONE);
        startSlave.setVisibility(View.GONE);
        distanceTextView.setVisibility(View.VISIBLE);
        startMeasure.setVisibility(View.VISIBLE);
    }

    public void setSlaveLayout() {
        startMaster.setVisibility(View.GONE);
        startSlave.setVisibility(View.GONE);
        stopSlave.setVisibility(View.VISIBLE);
    }

    public void setMainLayoutFromSlave() {
        stopSlave.setVisibility(View.GONE);
        startMaster.setVisibility(View.VISIBLE);
        startSlave.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode != AUDIO_ECHO_REQUEST)
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 1 ||
                grantResults[0] != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
    }

    public native boolean getDistance(TextView distanceTextView);
    public native boolean startMeasure();
    public native boolean stopSlave();
    public native boolean startSlave();
}
